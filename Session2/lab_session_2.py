# -*- coding: utf-8 -*-
"""
Created on Tue Oct 28 00:11:42 2014

@author: yves-laurent
"""
import numpy as np
from scipy.linalg import toeplitz
import pylab as plt

"""
Snippet 1: Load the matrix.
"""
import scipy.io as sio
mat = sio.loadmat('fXSamples.mat')
fx_data = mat['x']

x0 = fx_data[:, 0]
x1 = fx_data[:, 1]
x2 = fx_data[:, 2]
p = 4


"""
Snippet 2: Estimate AR coefficient using OLS
"""
def OLSA(x, p):
    # First reverse the input vector
    x_r = x[::-1]
    # Compute the response vector
    y = x_r[:-p]
    n = len(x)
    
    # Compute the design matrix
    M = np.ones((n-p, p))
    for i in range(1, p):
        M[:, i-1] = x_r[i:-p+i]
    M[:, p-1] = x_r[p:]
    
    # a = (M^TM)^-1M^T y
    a = np.dot(np.dot(np.linalg.inv(np.dot(M.T, M)), M.T), y)
    
    # Errors
    e = y - np.dot(M, a)
    rmse = (np.linalg.norm(e)/np.sqrt(len(e)))/np.std(e)
    mae = (np.linalg.norm(e, 1)/len(e))/np.std(e)
    
    return a, rmse, mae

print ''
print '####### OLS results #######'
ols_a, ols_rmse, ols_mae = OLSA(x0, p)
print 'OLS a', ols_a
print 'OLS RMSE', ols_rmse
print 'OLS MAE', ols_mae

"""
Snippet 2: Estimate AR coefficient by solving the Yule-Walker equation
"""
def YWA(x, p):
    # First reverse the input vector
    x_r = x[::-1]
    # Compute the response vector
    y = x_r[:-p]
    n = len(x)
    
    # Compute the design matrix
    M = np.ones((n-p, p+1))
    for i in range(1, p):
        M[:, i] = x_r[i:-p+i]
    M[:, p] = x_r[p:]
    
    # Set the constant term to y
    M[:,0] = y
    
    # Compute the covariance matrix between x(t), x(t-1), ..., x(t-p)
    cov = np.cov(M.T)
    cov_coeff = cov[0,:]
    ylw_matrix = toeplitz(cov_coeff[:-1], cov_coeff[:-1])
    ylw_y = cov_coeff[1:]
    
    # Solve the Yule-Walker equation
    a = np.linalg.solve(ylw_matrix, ylw_y)
    
    
    # Errors ?

    return a
    
yw_a = YWA(x0, p)
print ''
print '####### Yule-Walker results #######'
print 'YW a', yw_a

    
"""
Snippet 3: Estimate Power-Spectral Density
"""
def plotPowerSpectralDensity(a, var, ns):
    import cmath
#    Get spectrumm from AR coefficients
#    a   Vector of AR coefficients
#    var Variance of the noise term
#    ns  Samples per second
    spec = []
    p = len(a)
    ff = 0
    for w in np.arange(np.pi/128, np.pi*(1.0+1.0/128), np.pi/128):
        ff += 1
        denom = 0
        for k in np.arange(p):
            denom += a[k]*cmath.exp(-k*w*1j)
        denom += 1
        spec += [1.0/(np.abs(denom)**2)]
        
    spec = var*np.array(spec)
    freq = np.arange(0.5*ns/128.0, 0.5*ns*(1.0+1.0/128), 0.5*ns/128)
    plt.figure()
    plt.plot(freq, spec, '-r')
    plt.xlabel('f')
    plt.ylabel('S(f)')
    
    return (freq, spec)

print "Ploting the spectrum from the AR"
plotPowerSpectralDensity(yw_a, np.var(x0), 1)