\documentclass[a4paper,11pt]{article}

\usepackage{epsfig} \usepackage{amsfonts} \usepackage{times}
\usepackage{graphicx} \usepackage{pstricks} \usepackage{subfigure}
\usepackage{epsfig} \usepackage{url} \usepackage{array}
\usepackage{amsfonts} \usepackage{amsmath} \usepackage{amsthm}
\usepackage{amssymb} \usepackage{latexsym} \usepackage{float}
\usepackage{multirow} \usepackage[latin1]{inputenc}
\usepackage{color,psfrag} \usepackage{pstool}
%\usepackage{subfig}
\usepackage{pst-all} % PSTricks \usepackage{natbib}
\usepackage{nicefrac}

%\usepackage{cmbright}

\sloppy

% Definition of simplified commands
\newcommand{\ben}{\begin{enumerate}}
\newcommand{\een}{\end{enumerate}} \newcommand{\bi}{\begin{itemize}}
\newcommand{\ei}{\end{itemize}} \newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}} \newcommand{\bea}{\begin{eqnarray}}
\newcommand{\eea}{\end{eqnarray}} \newcommand{\ba}{\begin{array}}
\newcommand{\ea}{\end{array}} \newcommand{\bc}{\begin{center}}
\newcommand{\ec}{\end{center}} \newcommand{\bt}{\begin{tabular}}
\newcommand{\et}{\end{tabular}}
\newcommand{\bfig}{\begin{figure}[htb]}
\newcommand{\efig}{\end{figure}} \newcommand{\cl}{\centerline}
\newcommand{\dstyle}{\displaystyle}
\newcommand{\vs}{\vspace{2cm} \noindent}
\newcommand{\svs}{\vspace{1cm} \noindent} \newcommand{\np}{\newpage}
\newcommand{\bmg}[1]{\mbox{\boldmath $#1$}} \newcommand{\bm}[1]{
  \mathbf{#1} } \newcommand{\ul}[1]{\underline{#1} }
\newcommand{\picin}[2]{\mbox{\epsfig{file=#1,width=#2cm}}}
\newcommand{\defin}{\stackrel{\mbox{\tiny def}}{=}}
\newcommand{\dint}{\int \!\! \int} \newcommand{\nnl}{\nonumber \\}
\newcommand{\tr}{^{\sf T}} \newcommand{\dnt}[1]{_{{#1}}}
\newcommand{\Kl}[1]{K_{\text{#1}}}

% now some commands specific to this paper
\newcommand{\Gauss}[1]{\mathcal{N}(#1)}
\newcommand{\vphi}{\bmg{\varphi}} \newcommand{\given}{\ensuremath{\; |
    \;}} \newcommand{\ls}{\lambda} \newcommand{\hs}{h}

% now some newcommands from mosb
\newcommand{\vect}[1]{\boldsymbol{#1}}
% for lines that need to be skinny -- rg
\newcommand{\mat}[1]{\mathbf{#1}}
\newcommand{\pderiv}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\npderiv}[2]{\nicefrac{\partial #1}{\partial #2}}
\newcommand{\pha}{^{\phantom{:}}}
\newcommand{\argmin}{\operatornamewithlimits{argmin}}
\newcommand{\argmax}{\operatornamewithlimits{argmax}}
\newcommand{\ud}{\mathrm{d}}
% The following designed for probabilities with long arguments
\newcommand{\Prob}[2]{P\!\left(\,#1\;\middle\vert\;#2\,\right)}
\newcommand{\ProbF}[3]{P\!\left(\,#1\!=\!#2\;\middle\vert\;#3\,\right)}
\newcommand{\p}[2]{p\!\left(#1\middle\vert#2\right)}

%Single spacing
\newcommand{\ssp}{\setlength{\baselineskip}{13.6truept}}
%Double spacing
\newcommand{\dsp}{\setlength{\baselineskip}{25.0truept}}

\textwidth=17cm \textheight=22cm \oddsidemargin=-.5cm
\evensidemargin=0cm \topmargin=0cm

\begin{document}

%\dsp
% paper title
\title{Lab Session 2\\
Auto- \& cross-regressive models \& least-squares solutions}
\date{}

% make the title area
\maketitle

\noindent In this session we will be looking into developing \emph{(auto)regressive models}. Recall that the generic form of the model is
\[
\hat{y}[t] = \sum_{i=1}^{p} a_i y[t-i]
\]

Recall that we can solve for the unknown set of coefficients, $\{a_i\}$, in several ways
\begin{enumerate}
\item By forming the \emph{embedding} matrix, $\bf{M}$, from lagged versions of a data series. As $\bf{M}$, in general is non-square, we will have to use the pseudo-inverse, i.e. evaluate $\bf({M}^T\bf{M})^{-1}\bf{M}^T$. 
\item Estimate the autocorrelation matrix, $\bf{R}$, and use $\bf{R}^{-1}$ to infer the coefficients.
\end{enumerate}

\paragraph{Preamble} Load the data, provided at 
\begin{verbatim}
http://www.robots.ox.ac.uk/~aims-cdt/Outgoing/Session2
\end{verbatim}
This consists of three data samples of financial time series (foreign exchange rates) sampled once per second. These are shown below.
\begin{figure}
\vspace*{-6cm}
\cl{ \picin{finDat.eps}{10} }
\caption{Some financial data.}
\label{fig:findat}
\end{figure}

\newpage
\paragraph{Direct 1-d solutions}
We start out by looking at one of these time series - up to you which one.
\bi
\item By evaluating the least-squares solution for $\bf{a}$ using the embedding method, write code to infer the coefficients.
\item Re-evaluate the solution for $\bf{a}$ using a direct inversion of the autocorrelation matrix.
\item \emph{[optional] Use a Toeplitz solver - the Yule-Walker recursions, for example, to evaluiate the coefficients rather than a direct inversion of the autocorrelation matrix. For large model orders, $p >> 1$, can you notice a difference in speed?}
\item As the AR models represent \emph{one-step prediction} models, you can see how well they do at forecasting - would you make money from this?
\ei

\paragraph{Spectral estimates}
It is pretty straight-forward now to write code to evaluate the spectrum from the AR coefficients. You may want to experiment with the AR order and see what effect this has on the spectral estimation.

\paragraph{Cross-regression}
\bi
\item Modify the embedding matrix method, so that
\[
\hat{y}[t] = \sum_{i=1}^{p} a_i z[t-i]
\]
in which $y$ is being modelled by observing another timeseries, $z$. The coefficients of this model now describe the cross-regression i.e. how the past of $z$ effects the present value of $y$. We can look at the magnitude of the coefficients as well as the predictions of $y$ to give an idea about the information that `flows' from $z$ to $y$. Do any of the timeseries have strong interactions? If so, is there any indication of which one is driving which?
\item These coefficients give us some information regarding the spectral modes (resonances) in $y$ which can be `explained' by observing $z$.
\ei

\paragraph{Regularisation} [optional] If you have the time, you can look at regularising the solutions of the least-squares linear models. From a \emph{Bayesian} perspective this is equivalent to putting \emph{priors} on the coefficients which penalise large values. A simple method to achieve this is \emph{shrinkage}, which adds `jitter', of magnitude $\alpha$ say along the leading diagonal of anything we invert - in other words $\bf{A}^{-1}$ becomes
$(\bf{A} +  \alpha \bf{I})^{-1}$.
This has the effect of helping reduce rank-deficiency in $\bf{A}$ and has the knock on, in our system, of \emph{shrinking} the values of $\bf{a}$ towards zero. If you get a chance try it, and see what effect it has. You should be able to obtain coefficients for model orders close to the number of data points without getting singular solutions.
\end{document}
