function [a,v,y,y_pred] = arls2(Z,Y,p,reg);

%  function [a,v,y,ypred] = arls(Z,Y,p,reg);
%  Least squares AR algorithm
%  y_pred (t) = - \sum_i a_i z (t-i) + e (t)
%  Note the sign and ordering. This to keep format with ar_spectra.
%  Z,Y    univariate time series
%  p      order of model
%  reg    regularisation constant
%  a      AR coefficients
%  v      variance of residuals
%  y      targets
%  y_pred predictions

if nargin < 2, error('arls needs at least two arguments'); end

[M,y] = arembed2(Z,Y,p);
M = -M; % just corrects for the -ve in the standard form AR equations
I = eye(p);
R = (M'*M + reg*I);
a = inv(R)*M'*y;
%a = pinv(-1*x)*y;
y_pred = M*a;
v=mean((y-y_pred).^2);
