# -*- coding: utf-8 -*-
"""
Created on Sat Oct 25 23:02:36 2014

@author: yves-laurent
"""

from pylab import plot, show, xlabel, ylabel, subplot, figure
from scipy import fft, arange, ifft
from scipy.io import wavfile
import numpy as np

def plotSpectrum(y,Fs):
    """
    Plots a Single-Sided Amplitude Spectrum of y(t)
    :param y: the signal
    :param Fs: the sampling frequency
    """
    n = len(y) # length of the signal
    k = arange(n)
    T = n/Fs 
    frq = k/T # Two sides frequency range
    frq = frq[range(n/2)] # One side frequency range
    Y = fft(y)/n # FFT computing and normalization
    Y = Y[range(n/2)]
    figure()
    # Plot the signal in wall-clock time
    subplot(2,1,1)
    Ts = 1.0/Fs; # sampling interval
    t = arange(0, 1.0*len(y)/Fs, Ts)
    plot(t, y)
    xlabel('Time')
    ylabel('Amplitude')
    # Plot the spectrum 
    subplot(2,1,2)
    plot(frq, np.log(abs(Y)),'r') # Plotting the spectrum
    xlabel('Freq (Hz)')
    ylabel('log|Y(freq)|')
    show()
    

def saveWav(y, name, Fs=44100):
    """
    Save a signal y as a wave file
    :param y: the signal
    :param name: the name of the file
    :param rate: the sampling frequency
    """
    wavfile.write(name, Fs, y)


"""
Below are a couple of code snippets. When needed, make sure that the audio file
is on your Python path (typically you might want to put it in the same directory as this file).
"""


"""
Snippet 1: Read a .wav file. Uncomment the below to use.
"""
#file_name = 'file.wav'
#Fs, y = wavfile.read(file_name)


"""
Snippet 2: Plot a single-sided spectrum. Uncomment the below to use.
"""
#file_name = 'file.wav'
#Fs, y = wavfile.read(file_name)
#plotSpectrum(y, Fs)

"""
Snippet 3: Save a .wav file. Uncomment the below to use, run and check the current directory.
"""
#file_name = 'file.wav'
#Fs, y = wavfile.read(file_name)
#y_noisy = y + np.random.randn(len(y))
#saveWav(y, 'noisy_' + file_name, Fs=Fs)

"""
Snippet 4: Recover a signal from its DFT. Uncomment the below to use.
"""
#file_name = 'file.wav'
#Fs, y = wavfile.read(file_name)
#F = fft(y)
#y_recov = ifft(F)




