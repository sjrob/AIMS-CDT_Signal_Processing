% Snippet 1: Read a .wav file. Uncomment the below to use.
% [y, Fs] = audioread('file.wav');

% Snippet 2: Plot a single-sided spectrum. Uncomment the below to use.
% [y, Fs] = audioread('file.wav');
% plotSpectrum(y, Fs);

% Snippet 3: Play a sound. Uncomment the below to use.
% [y, Fs] = audioread('file.wav');
% sound(y, Fs);
   
% Snippet 4: Recover a signal from its DFT. Uncomment the below to use.
% [y, Fs] = audioread('file.wav');
% DFT = fft(y);
% y_recov = ifft(DFT);
% sound(y_recov);
