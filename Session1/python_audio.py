import numpy as np
import scikits.audiolab as al
import scipy.io as scio
from scipy.signal import resample

audio = scio.loadmat('hum_remove_2.mat')
s_noisy = audio['s_noisy']
def al_play(snd, fs):
    if len(snd.shape) == 2:
        if snd.shape[1] == 2 or snd.shape[1] == 1:
            snd = snd.T
        elif snd.shape[0] != 2 and snd.shape[0] != 1:
            print "sound must either a vector or a rank 2 matrix"
            return
        N = snd.shape[1]
    else:
        N = snd.shape[0]
        snd = snd.reshape(-1, N)
    rsmple = resample(snd, N * 44100.0 / (1.0 * fs), axis=1)
    al.play(rsmple, fs=44100)

al_play(s_noisy, 9000)
