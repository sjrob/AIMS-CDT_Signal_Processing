function []= plotSpectrum(y, Fs)
    n = length(y);
    k = [0:n-1];
    T = n/Fs;
    frq = k/T;
    frq = frq(1:floor(n/2));
    Y = fft(y)/n;
    Y = Y(1:floor(n/2));
    Ts = 1.0/Fs;
    t = [0.0:Ts:length(y)/Fs-Ts];
    figure
    subplot(2,1,1);
    plot(t, y)
    xlabel('Time')
    ylabel('Amplitude')
    
    subplot(2,1,2);
    plot(frq, log(abs(Y)));
    xlabel('Freq (Hz)')
    ylabel('log|Y(freq)|')