\documentclass[]{article}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
% use microtype if available
\IfFileExists{microtype.sty}{\usepackage{microtype}}{}
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \usepackage{fontspec}
  \ifxetex
    \usepackage{xltxtra,xunicode}
  \fi
  \defaultfontfeatures{Mapping=tex-text,Scale=MatchLowercase}
  \newcommand{\euro}{€}
    \setmainfont{Helvetica Neue Light}
    \setmonofont{Consolas}
\fi
\usepackage[margin=1.5in]{geometry}
\usepackage{color}
\usepackage{fancyvrb}
\DefineShortVerb[commandchars=\\\{\}]{\|}
\DefineVerbatimEnvironment{Highlighting}{Verbatim}{commandchars=\\\{\}}
% Add ',fontsize=\small' for more characters per line
\usepackage{framed}
\definecolor{shadecolor}{RGB}{248,248,248}
\newenvironment{Shaded}{\begin{snugshade}}{\end{snugshade}}
\newcommand{\KeywordTok}[1]{\textcolor[rgb]{0.13,0.29,0.53}{\textbf{{#1}}}}
\newcommand{\DataTypeTok}[1]{\textcolor[rgb]{0.13,0.29,0.53}{{#1}}}
\newcommand{\DecValTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{{#1}}}
\newcommand{\BaseNTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{{#1}}}
\newcommand{\FloatTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{{#1}}}
\newcommand{\CharTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{{#1}}}
\newcommand{\StringTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{{#1}}}
\newcommand{\CommentTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textit{{#1}}}}
\newcommand{\OtherTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{{#1}}}
\newcommand{\AlertTok}[1]{\textcolor[rgb]{0.94,0.16,0.16}{{#1}}}
\newcommand{\FunctionTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{{#1}}}
\newcommand{\RegionMarkerTok}[1]{{#1}}
\newcommand{\ErrorTok}[1]{\textbf{{#1}}}
\newcommand{\NormalTok}[1]{{#1}}
\usepackage{ctable}
\usepackage{float} % provides the H option for float placement
\usepackage{graphicx}
% We will generate all images so they have a width \maxwidth. This means
% that they will get their normal width if they fit onto the page, but
% are scaled down if they would overflow the margins.
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth
\else\Gin@nat@width\fi}
\makeatother
\let\Oldincludegraphics\includegraphics
\renewcommand{\includegraphics}[1]{\Oldincludegraphics[width=\maxwidth]{#1}}
\ifxetex
  \usepackage[setpagesize=false, % page size defined by xetex
              unicode=false, % unicode breaks when used with xetex
              xetex]{hyperref}
\else
  \usepackage[unicode=true]{hyperref}
\fi
\hypersetup{breaklinks=true,
            bookmarks=true,
            pdfauthor={},
            pdftitle={},
            colorlinks=true,
            urlcolor=blue,
            linkcolor=magenta,
            pdfborder={0 0 0}}
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\setcounter{secnumdepth}{0}

\author{}
\date{}

\begin{document}

\section{State space models (Lectures 5 and 6)}

In this practical you will derive and implement a Kalman filter for
various estimation tasks. There are three parts, however you aren't
expected to complete all of them in the time available.

\subsection{Part 1 - Adaptive Linear Regression}

Your lecture notes give an example of an adaptive linear regressor (or
filter). There is a vector of weights $\mathbf{w}$, used to predict a
noisy output $y$.

\begin{figure}[htbp]
\centering
\includegraphics{kf1d_signal.png}
\caption{Noisy data}
\end{figure}

This data can be found at \footnote{\url{http://www.robots.ox.ac.uk/~trn/kf1d_signal.mat}}.
Here, $y$ is a function of time, so your prediction equation will be

\[
\bar{y} = \mathbf{w}_t^T \mathbf{T}_{t, t - l} 
\]

where $\mathbf{T}$ is the vector of all times, $l$ is the length of the
weight vector and $\mathbf{T}_{t : t-l}$ is the vector of the $l$ most
recent values of $\mathbf{T}$.

As usual, before you start coding you should write down the equations
you are attempting to implement. Luckily for you these are in the
lecture notes. Now code it up in MATLAB or Python.

Play around with the signal noise ($\mathbf{R}$), the process noise
($\mathbf{Q}$) and the length of your filter. A reasonable starting
value of the process noise is around $10^{-7}$, and the signal noise is
$10$. Try and find the best combination of values to filter out the
noise and match the true signal.

\subsection{Part 2 - Tracking}

There is a madman racing a boat around in a major shipping lane.
Unfortunately, the radar used to track boats is inaccurate and regularly
drops out. The harbour master offers you \$\$\$ if you can find a way to
make up for the deficiencies in the radar.

\begin{figure}[htbp]
\centering
\includegraphics{boat_path.png}
\caption{Boat track}
\end{figure}

You are given a .mat of historical data\footnote{\url{http://www.robots.ox.ac.uk/~trn/kf2d_signal.mat}}
gathered over the last 15 minutes or so (1000 seconds). There are six
vectors:

\ctable[pos = H, center, botcap]{ll}
{% notes
}
{% rows
\FL
Name & Description
\ML
t\_n & Corrupted time vector. Missing some values.
\\\noalign{\medskip}
x\_n & Missing the same values as t\_n, also additive gaussian noise
with variance 30.
\\\noalign{\medskip}
y\_n & Missing the same values as t\_n, also additive gaussian noise
with variance 30.
\\\noalign{\medskip}
t & Full time vector.
\\\noalign{\medskip}
x & Full x vector for testing.
\\\noalign{\medskip}
y & Full y vector for testing.
\LL
}

You are expected to return, for each time step between 0 and 1000, the
mean and variance prediction of your KF. You are given the full $x$, $y$
and $t$ vectors for testing purposes.

\subsubsection{Design of KF}

A strength of the KF is that you can explicitly model the dynamics of
your system. Think about the dynamics of the boat - the two controls are
the throttle and the rudder. The throttle controls the boat's velocity,
and the rudder angle and the boat's velocity combined control the rate
of rotation. Then, $\dot{x} = v \cos(\theta)$ and
$\dot{y} = v \sin(\theta)$.

While it is possible to make a very rich model of the boat, we will
start with a \textbf{constant velocity model}. This assumes that the
boat's state can be adequately represented by
$[x, y, \dot{x}, \dot{y}]$, and acceleration is absorbed into the
process noise.

Before coding, start by writing down the plant model ($\mathbf{F}$), the
observation model ($\mathbf{G}$) along with the process noise model
($\mathbf{J}$) and the observation noise model ($\mathbf{V}$) for this
simple KF. What values will you use for the observation and process
noise?

Once you have done this, try a richer model such as the constant
acceleration (state is $[x, y, \dot{x}, \dot{y}, \ddot{x}, \ddot{y}]$)
or an EKF\footnote{\url{http://en.wikipedia.org/wiki/Extended_Kalman_filter}}
based model taking into account the non-linear dynamics of the boat.

For anyone feeling very adventurous, there is an elegant link between
the KF and \emph{Gaussian Processes}, which you have already been
working with. Some details of these relationships, along with the form
of \emph{kernel function} for a GP associated with e.g. constant
velocity and constrant acceleration KF models can be found in recent papers.\footnote{\url{http://www.robots.ox.ac.uk/~sjrob/Pubs/cam_gp.pdf}}\footnote{\url{http://www.robots.ox.ac.uk/~sjrob/Pubs/Fusion2010_0132.pdf}.}

\subsection{Part 3 - Lagged Kalman Filter}

Finally, derive the plant and observation models for the lagged Kalman
filter for correlation shown in your lecture notes (Lecture 5, ``Full
state-space model''). You can find some data at\footnote{\url{http://www.robots.ox.ac.uk/~trn/brownian_data.mat}}
or just generate your own:

\textbf{Python:}

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{np.cumsum(np.random.randn(}\DecValTok{6000}\NormalTok{, ))}
\end{Highlighting}
\end{Shaded}

\textbf{Matlab:}

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{cumsum(randn(}\FloatTok{6000}\NormalTok{,}\FloatTok{1}\NormalTok{))}
\end{Highlighting}
\end{Shaded}

\end{document}
