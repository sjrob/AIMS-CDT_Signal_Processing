
% Example 1: Function smoothing

t = [0.0:0.1:100.0];
dt = 0.1;
x = sin(exp(t * 0.03)) .* exp(-t.^2 / 10000.0);
train_x = x + 0.1*randn(size(x));
train_t = t;
F = double([1.0 dt; 0.0 1.0]);
H = [1.0 0.0];
G = [0.5*(dt^2) dt];
Q = 0.01*(G'*G);
R = [0.01];
x_post = double([train_x(1); (train_x(2)-train_x(1))/(train_t(2)-train_t(1))]);
P_post = 100.0*eye(2);

[pred, pred_var] = kalmanSmoother(train_x, train_t, F, H, Q, R, x_post, P_post);
figure()
plot(t, x, '-', t, pred, '*', t, train_x, '*')
legend('True','KF', 'Noisy')
title('Function smoothing')

% Example 2: Trajectory tracking with constant acceleration
%   Gerono lemniscate trajectory under the constant (noisy) accelaration
%   assumption

t = [0:0.01:2*pi];
x = 100.0*sin(t);
y = 100.0*sin(t).*cos(t);

train_x = x + 5.0*randn(size(x));
train_y = y + 5.0*randn(size(y));
train_t = t;

% Construct the filter
dt = 0.01;
dnoise = 10.0; % Variance of the accelaration
onoise = 0.01; % Variance of the observation noise
F = double([1.0 dt 0.0 0.0; 0.0 1.0 0.0 0.0; 0.0 0.0 1.0 dt; 0.0 0.0 0.0 1.0]);
H = [1.0 0.0 0.0 0.0; 0.0 0.0 1.0 0.0];
G = [0.5*(dt^2) dt 0.5*(dt^2) dt];
Q = dnoise*[(dt^4)/4 (dt^3)/2 0.0 0.0; (dt^3)/2 dt^2 0.0 0.0; 0.0 0.0 (dt^4)/4 (dt^3)/2; 0.0 0.0 (dt^3)/2 dt^2];
R = [onoise^2 0.0; 0.0 onoise^2];
x_post = double([train_x(1); (train_x(2)-train_x(1))/(train_t(2)-train_t(1));train_y(1);(train_y(2)-train_y(1))/(train_t(2)-train_t(1))]);
P_post = 100*eye(4);

% Training data
train_data = [train_x; train_y];

% Predictive mean and variance from Kalman Filter
[pred, pred_var] = kalmanSmoother(train_data, train_t, F, H, Q, R, x_post, P_post);
x_pred = pred(1,:);
y_pred = pred(2,:);

% Plots
figure()
plot(x, y, '-', x_pred, y_pred, '*', train_x, train_y, '*')
legend('True','KF', 'Noisy')
title('Tracking with constant acceleration')

% Example 3: Trajectory tracking with known acceleration and constant jerk

%   Gerono lemniscate trajectory under the constant (noisy) jerk
%   assumption.
%   In this example, x(t) = sin(t), y(t) = sin(t)cos(t). Hence, we can
%   include in the dynamics that the accelerations are x''(t) = -sin(t)
%   =-x(t) and y''(t)=-4y(t). We take [x(t), x'(t), y(t), y'(t)] to be the
%   unknown as before, and take the jerks x'''(t), y'''(t) to be noise of
%   our system.

%  Note the improve accuracy!

t = [0:0.01:2*pi];
x = 100.0*sin(t);
y = 100.0*sin(t).*cos(t);

train_x = x + 5.0*randn(size(x));
train_y = y + 5.0*randn(size(y));
train_t = t;

% Construct the filter
dt = 0.01;
dnoise = 10.0; % Dynamics noise
onoise = 0.01; % Observation noise
F = double([1.0-0.5*(dt^2) dt 0.0 0.0; -dt 1.0 0.0 0.0; 0.0 0.0 1.0-2*(dt^2) dt; 0.0 0.0 -4.0*dt 1.0]);
H = [1.0 0.0 0.0 0.0; 0.0 0.0 1.0 0.0];
G = [1.0/6.0*(dt^3) 1.0/2.0*(dt^2) 1.0/6.0*(dt^3) 1.0/2.0*(dt^2)];
Q = [(1.0/6.0*(dt^3))^2 1.0/12.0*(dt^5) 0.0 0.0;1.0/12.0*(dt^5) (1.0/2.0*(dt^2))^2 0.0 0.0; 0.0 0.0 (1.0/6.0*(dt^3))^2 1.0/12.0*(dt^5); 0.0 0.0 1.0/12.0*(dt^5) (1.0/2.0*(dt^2))^2];
R = [onoise^2 0.0; 0.0 onoise^2];
x_post = double([train_x(1); (train_x(2)-train_x(1))/(train_t(2)-train_t(1));train_y(1);(train_y(2)-train_y(1))/(train_t(2)-train_t(1))]);
P_post = 100*eye(4);

% Training data
train_data = [train_x; train_y];

% Predictive mean and variance from Kalman Filter
[pred, pred_var] = kalmanSmoother(train_data, train_t, F, H, Q, R, x_post, P_post);
x_pred = pred(1,:);
y_pred = pred(2,:);

% Plots
figure()
plot(x, y, '-', x_pred, y_pred, '*', train_x, train_y, '*')
legend('True','KF', 'Noisy')
title('Tracking with known acceleration and constant jerk')
