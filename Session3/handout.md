# State space models (Lectures 5 and 6)

In this practical you will derive and implement a Kalman filter for various estimation tasks. There are three parts, however you aren't expected to complete all of them in the time available.

## Part 1 - Adaptive Linear Regression

Your lecture notes give an example of an adaptive linear regressor (or filter). There is a vector of weights $\mathbf{w}$, used to predict a noisy output $y$.

![Noisy data](kf1d_signal.png)

This data can be found at [^kf1d_data]. Here, $y$ is a function of time, so your prediction equation will be

$$
\bar{y} = \mathbf{w}_t^T \mathbf{T}_{t, t - l} 
$$

where $\mathbf{T}$ is the vector of all times, $l$ is the length of the weight vector and $\mathbf{T}_{t : t-l}$ is the vector of the $l$ most recent values of $\mathbf{T}$.

As usual, before you start coding you should write down the equations you are attempting to implement. Luckily for you these are in the lecture notes. Now code it up in MATLAB or Python.

Play around with the signal noise ($\mathbf{R}$), the process noise ($\mathbf{Q}$) and the length of your filter. A reasonable starting value of the process noise is around $10^{-7}$, and the signal noise is $10$. Try and find the best combination of values to filter out the noise and match the true signal.

## Part 2 - Tracking

There is a madman racing a boat around in a major shipping lane. Unfortunately, the radar used to track boats is inaccurate and regularly drops out. The harbour master offers you \$\$\$ if you can find a way to make up for the deficiencies in the radar.

![Boat track](boat_path.png)

You are given a .mat of historical data[^kf2d_data] gathered over the last 15 minutes or so (1000 seconds). There are six vectors:

Name    Description
------- ------------
t_n     Corrupted time vector. Missing some values.
x_n     Missing the same values as t_n, also additive gaussian noise with variance 30.
y_n     Missing the same values as t_n, also additive gaussian noise with variance 30.
t       Full time vector.
x       Full x vector for testing.
y       Full y vector for testing.

You are expected to return, for each time step between 0 and 1000, the mean and variance prediction of your KF. You are given the full $x$, $y$ and $t$ vectors for testing purposes.

### Design of KF

A strength of the KF is that you can explicitly model the dynamics of your system. Think about the dynamics of the boat - the two controls are the throttle and the rudder. The throttle controls the boat's velocity, and the rudder angle and the boat's velocity combined control the rate of rotation. Then, $\dot{x} = v \cos(\theta)$ and $\dot{y} = v \sin(\theta)$.

While it is possible to make a very rich model of the boat, we will start with a **constant velocity model**. This assumes that the boat's state can be adequately represented by $[x, y, \dot{x}, \dot{y}]$, and acceleration is absorbed into the process noise.

Before coding, start by writing down the plant model ($\mathbf{F}$), the observation model ($\mathbf{G}$) along with the process noise model ($\mathbf{J}$) and the observation noise model ($\mathbf{V}$) for this simple KF. What values will you use for the observation and process noise?

Once you have done this, try a richer model such as the constant acceleration (state is $[x, y, \dot{x}, \dot{y}, \ddot{x}, \ddot{y}]$) or an EKF[^EKF] based model taking into account the non-linear dynamics of the boat.

##Part 3 - Lagged Kalman Filter

Finally, derive the plant and observation models for the lagged Kalman filter for correlation shown in your lecture notes (Lecture 5, "Full state-space model"). You can find some data at[^brownian_data] or just generate your own: 

__Python:__

```{.python}
np.cumsum(np.random.randn(6000, ))
```

__Matlab:__

```{.matlab}
cumsum(randn(6000,1))
```

[^kf1d_data]: <http://www.robots.ox.ac.uk/~trn/kf1d_signal.mat>
[^kf2d_data]: <http://www.robots.ox.ac.uk/~trn/kf2d_signal.mat>
[^EKF]: <http://en.wikipedia.org/wiki/Extended_Kalman_filter>
[^brownian_data]: <http://www.robots.ox.ac.uk/~trn/brownian_data.mat>
