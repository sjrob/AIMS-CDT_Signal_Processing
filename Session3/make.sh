#! /bin/bash

pandoc --variable version=01  --highlight-style=tango --latex-engine=xelatex --variable mainfont="Helvetica Neue Light" --variable monofont=Consolas handout.md -o handout.pdf
