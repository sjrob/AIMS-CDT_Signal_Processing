function [x_pred, var_pred] = kalmanSmoother(train_data, train_t, F, H, Q, R, x_post, P_post)

% train_data: matrix of training data. The columns represent states at
%   different times.
% train_t: time vector corresponding to the training data.
% F: factor in the dynamics equation (x[k] = Fx[k-1] + w[k])
% H: Factor in the observation equation (z[k] = Hx[k] + v[k]
% Q: covariance matrix of the noise of the dynamics
% R: covariance matrix of the noise term in the observation equation
% x_post: initial value for the posterior prediction
% P_post: covariance matrix of the initial posterior prediction (how
% confident are we about the initial guess?)

% Initialise the vectors of predictions
x_pred = [];
var_pred = [];

% Run the KF updates
for tm=1:length(train_t),
    
    % Update the a priori prediction  and the corresponding error using the dynamics equation
    x_prio = F*x_post;
    P_prio = F*P_post*F' + Q;
    
    % Innovation from the a priori prediction
    e = train_data(:,tm) -H*x_prio;
    
    % Innovation covariance
    IC = H*P_prio*H' + R;
    
    % Optimal Gain
    K = P_prio*H'*inv(IC);
    
    % Compute a posteriori predictions
    x_post = x_prio + K*e;
    
    % Compute a posteriori error covariance
    P_post = (eye(length(x_prio))-K*H)*P_prio;
    
    % Update the vector of predictions
    x_pred = [x_pred H*x_post];
    var_pred = [var_pred diag(P_post)];
end
