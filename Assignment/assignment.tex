\documentclass[a4paper,11pt]{article}

\usepackage{epsfig} \usepackage{amsfonts} \usepackage{times}
\usepackage{graphicx} \usepackage{pstricks} \usepackage{subfigure}
\usepackage{epsfig} \usepackage{url} \usepackage{array}
\usepackage{amsfonts} \usepackage{amsmath} \usepackage{amsthm}
\usepackage{amssymb} \usepackage{latexsym} \usepackage{float}
\usepackage{multirow} \usepackage[latin1]{inputenc}
\usepackage{color,psfrag} \usepackage{pstool}
%\usepackage{subfig}
\usepackage{pst-all} % PSTricks \usepackage{natbib}
\usepackage{nicefrac}

%\usepackage{cmbright}

\sloppy

% Definition of simplified commands
\newcommand{\ben}{\begin{enumerate}}
\newcommand{\een}{\end{enumerate}} \newcommand{\bi}{\begin{itemize}}
\newcommand{\ei}{\end{itemize}} \newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}} \newcommand{\bea}{\begin{eqnarray}}
\newcommand{\eea}{\end{eqnarray}} \newcommand{\ba}{\begin{array}}
\newcommand{\ea}{\end{array}} \newcommand{\bc}{\begin{center}}
\newcommand{\ec}{\end{center}} \newcommand{\bt}{\begin{tabular}}
\newcommand{\et}{\end{tabular}}
\newcommand{\bfig}{\begin{figure}[htb]}
\newcommand{\efig}{\end{figure}} \newcommand{\cl}{\centerline}
\newcommand{\dstyle}{\displaystyle}
\newcommand{\vs}{\vspace{2cm} \noindent}
\newcommand{\svs}{\vspace{1cm} \noindent} \newcommand{\np}{\newpage}
\newcommand{\bmg}[1]{\mbox{\boldmath $#1$}} \newcommand{\bm}[1]{
  \mathbf{#1} } \newcommand{\ul}[1]{\underline{#1} }
\newcommand{\picin}[2]{\mbox{\epsfig{file=#1,width=#2cm}}}
\newcommand{\defin}{\stackrel{\mbox{\tiny def}}{=}}
\newcommand{\dint}{\int \!\! \int} \newcommand{\nnl}{\nonumber \\}
\newcommand{\tr}{^{\sf T}} \newcommand{\dnt}[1]{_{{#1}}}
\newcommand{\Kl}[1]{K_{\text{#1}}}

% now some commands specific to this paper
\newcommand{\Gauss}[1]{\mathcal{N}(#1)}
\newcommand{\vphi}{\bmg{\varphi}} \newcommand{\given}{\ensuremath{\; |
    \;}} \newcommand{\ls}{\lambda} \newcommand{\hs}{h}

% now some newcommands from mosb
\newcommand{\vect}[1]{\boldsymbol{#1}}
% for lines that need to be skinny -- rg
\newcommand{\mat}[1]{\mathbf{#1}}
\newcommand{\pderiv}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\npderiv}[2]{\nicefrac{\partial #1}{\partial #2}}
\newcommand{\pha}{^{\phantom{:}}}
\newcommand{\argmin}{\operatornamewithlimits{argmin}}
\newcommand{\argmax}{\operatornamewithlimits{argmax}}
\newcommand{\ud}{\mathrm{d}}
% The following designed for probabilities with long arguments
\newcommand{\Prob}[2]{P\!\left(\,#1\;\middle\vert\;#2\,\right)}
\newcommand{\ProbF}[3]{P\!\left(\,#1\!=\!#2\;\middle\vert\;#3\,\right)}
\newcommand{\p}[2]{p\!\left(#1\middle\vert#2\right)}

%Single spacing
\newcommand{\ssp}{\setlength{\baselineskip}{13.6truept}}
%Double spacing
\newcommand{\dsp}{\setlength{\baselineskip}{25.0truept}}

\textwidth=17cm \textheight=22cm \oddsidemargin=-.5cm
\evensidemargin=0cm \topmargin=0cm

\begin{document}

%\dsp
% paper title
\title{\vspace*{-3cm}Signal Processing: End of Module Open Assignment}
\date{}

% make the title area
\maketitle


\section*{Preamble}
There are a set of four time series in the {\tt PredData} directory of the following site:
\begin{verbatim}
http://www.robots.ox.ac.uk/~aims-cdt/Outgoing/Assignment
\end{verbatim}

\section*{The data}
The four data sets represent \emph{real} data -
our goal is ambitious, and will stretch in some data streams the
ability of \emph{any} model to perform good forecasting and
modelling. Please note that you are welcome to use any code you have
already written, such as Gaussian Processes as well as third-party code, e.g. for optimisation and inference routines, or other components of your models.\\
\\
Although there are four data sets here, please don't feel obliged to produce results for them all. The aim of this is to get experience with some real data, not to feel under pressure!
Do also remember that it's not just the final performance of any algorithm that's of
  importance here - getting some experience with what works and what
  doesn't, in the details of implementation (the tricks and the
  pitfalls) is all part of this.

\paragraph{Finance:} The data contained in {\tt finPredProb.mat}
  consists of a single stream of data, {\tt ttr}, taken once per
  second for 4 hours from a foreign exchange rate, as shown in Figure
  \ref{fig:findat}.
\begin{figure}[htb]
\cl{ \picin{finDat.eps}{8} }
\caption{Some financial data.}
\label{fig:findat}
\end{figure}

\noindent \textbf{The goal -} is to produce a sequential model to provide the most accurate
prediction into the future and quantify the prediction
performance. Don't be alarmed if there is very little
predictability.\footnote{If you find otherwise, please start a company
immediately!}

\paragraph{Mackey-Glass chaotic system:} This system is defined by the
chaotic differential feedback system of the following form
\[
\frac{dx}{dt} = \beta \frac{ x_{t-\tau} }{1+{x_{t-\tau}}^n}-\gamma x,
\quad \gamma,\beta,\tau, n > 0
\]
For different values of the parameters in this model we obtain some wildly chaotic behaviours.
\begin{figure}[htb]
\cl{ \picin{mg.eps}{8} }
\caption{The Mackey-Glass sequence. you can train on the blue and use
  this to forecast the red.}
\label{fig:mg}
\end{figure}

\noindent \textbf{The goal -} is to use the 800 samples of training data (in
       {\tt t\_tr}) to make accurate model forecasts about the next
       200 points (in {\tt t\_te}). Figure \ref{fig:mg} shows the full
       sequence, with training in blue and the out of sample test set in red.

\paragraph{Sunspot data:} This data set is somewhat of a classic data set, having been used in many analyses looking at detecting trends and cycles at multiple timescales. The data is monthly.
\begin{figure}[htb]
\cl{ \picin{sunspots.eps}{8} }
\caption{Sunspot data.}
\label{fig:ss}
\end{figure}

\noindent \textbf{The goal -} is to use the historic data up to develop a forecast for the year ahead. You can use all the data up to 1900 as a training set if needed, but then create a rolling regression to forecast 12 samples ahead each time.

\paragraph{$\mathrm{CO}_2$ data:} This is a recent data set of the atmospheric levels on a monthly basis.
\begin{figure}[htb]
\cl{ \picin{co2.eps}{8} }
\caption{$\mathrm{CO}_2$ data.}
\label{fig:co2}
\end{figure}

\noindent \textbf{The goal -} go on, frighten yourself (or not) by forecasting what levels will be between now and 2050.

\section*{Submission}
Please package up all code and a short report (a few pages perhaps), detailing the approaches taken and performances reached, into an archive file (e.g. zip file) and email to {\tt sjrob@robots.ox.ac.uk} by 6pm Monday 3rd November 2014.


\end{document}
